# Camera Calibration

This repository contains the solution for the 2nd proyect of the Computer Vision course of ITCR's Electronics Master program

## Image Capture

Displays the camera from video0 devices and if a chessboard pattern is found it will save the image to the given directory

### Usage:
```
./image_capture
```

### Requirements 
* Python3
* OpenCV

## Camera Calibration

Calibrates a camera from images in a given directory. Saves the result to a file with the given name.

### Usage
```
./calibrate_camera.py
```

### Requirements
* Python3
* OpenCV

## Virtual Reality

When it find the chessboard pattern it displays it axis overlay on it on a
screen. And it projects to a fixed location on another one while showing the
camera displacement and rotation.

If nothing found just displays the camera output.

### Requirements
* Python3
* OpenCV
* Numpy