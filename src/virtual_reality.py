#!/usr/bin/env python3

import argparse
import sys

import cv2
import numpy as np


def get_object_points(pattern_shape, pattern_width, pattern_height):
    """Returns a 2D grid corresponding to the object's 3D distribution

    Args:
        pattern_shape (int, int): Shape of the chessboard in number of squares
        pattern_width (int): Width of each square in mm
        pattern_height (int): Height of each square in mm

    Returns:
        [type]: [description]
    """
    real_world_points = np.zeros(
        (pattern_shape[0] * pattern_shape[1], 3), np.float32)
    real_world_points[:, :2] = np.mgrid[0:pattern_shape[0],
                                        0:pattern_shape[1]].T.reshape(-1, 2)

    real_world_points[:, 0] *= pattern_width
    real_world_points[:, 1] *= pattern_height

    return real_world_points


def get_axis(axis_lenght, inverted=True):
    """Returns the 3D coordinates of the axis points

    Args:
        axis_lenght (int): lenght of each axis in mm
        inverted (bool, optional): True if the Z axis should be inverted. Defaults to False.

    Returns:
        np.array: Numpy array with the 3D axis points
    """
    if inverted:
        return np.float32([[0, 0, 0], [axis_lenght, 0, 0], [0, axis_lenght, 0], [
                          0, 0, -axis_lenght]]).reshape(-1, 3)
    else:
        return np.float32([[0, 0, 0], [axis_lenght, 0, 0], [0, axis_lenght, 0], [
                          0, 0, axis_lenght]]).reshape(-1, 3)


def draw_axis(img, axis_points):
    """Draws a 3D axis on an image

    Args:
        img (np.array): Image where the axis will be drawn
        axis_points (np.array): 3D axis points, the first should be the origin and the next
    3 points should be the axis endpoints.

    Returns:
        np.array: Image with the axis drawn on it.
    """
    origin = tuple(axis_points[0].ravel())
    img = cv2.line(img, origin, tuple(axis_points[1].ravel()), (255, 0, 0), 5)
    img = cv2.line(img, origin, tuple(axis_points[2].ravel()), (0, 255, 0), 5)
    img = cv2.line(img, origin, tuple(axis_points[3].ravel()), (0, 0, 255), 5)

    return img


def get_chessboard_pattern(pattern_shape, pattern_width, pattern_height):
    """Returns a 2D array with a chessboard pattern on it

    Args:
        pattern_shape (int, int): Shape of the chessboard in number of squares
        pattern_width (int): Width of each square in mm
        pattern_height (int): Height of each square in mm

    Returns:
        np.array: Array with the chessboard on it
    """
    img = np.full((pattern_width * pattern_shape[0] + 1,
                   pattern_height * pattern_shape[1] + 1,
                   3), 255, dtype=np.uint8)

    object_points = get_object_points(
        pattern_shape, pattern_width, pattern_height)

    # Draw black over the correct frames
    for corner in object_points:
        corner = corner.astype(int)
        if ((corner[0] / pattern_width + corner[1] / pattern_height) % 2) != 0:
            img[corner[0]:(corner[0] + pattern_width), corner[1]:(corner[1] + pattern_height), :] = 0

    return img


def homography_from_vecs(rot_vec, trans_vec, camera_matrix, dist_coeffs):
    """Converts a rotation and translation vectors into a homography for a given camera calibration

    Args:
        rot_vec (np.array): 3x1 Rodrigues rotation vector
        trans_vec (np.array): 3x1 translation vector
        camera_matrix ([type]): Camera calibration matrix
        dist_coeffs ([type]): Camera calibration parameters

    Returns:
        np.array: 3x3 homography
    """
    # At least 4 points are needed for the homography, these are used assumed
    # in a plane
    object_points = np.array(
        [
            [0, 0, 0],
            [0, 1, 0],
            [1, 0, 0],
            [1, 1, 0],
        ], np.float32
    )

    image_points, _ = cv2.projectPoints(
        object_points,
        rot_vec,
        trans_vec,
        camera_matrix, dist_coeffs
    )

    # Copy is necessary since otherwise OpenCV will not understand the new
    # matrix as the correct size
    object_points_2d = object_points[:, 0:2].copy()
    image_points = np.reshape(image_points, object_points_2d.shape)

    return cv2.getPerspectiveTransform(object_points_2d, image_points)


def get_chessboard_vecs():
    """Returns the virtual reality translation and rotation vectors for the projected chessboard

    Returns:
        np.array, np.array: 3x1 rotation vector and 3x3 translation vector
    """
    # Empirically determined rotation angles
    yaw = np.radians(90)
    pitch = np.radians(75)

    rot_mat = np.array(
        [
            [np.cos(yaw), -np.sin(yaw), 0],
            [np.sin(yaw), np.cos(yaw), 0],
            [0, 0, 1]
        ],
        np.float32
    ) @ np.array(
        [
            [np.cos(pitch), 0, np.sin(pitch)],
            [0, 1, 0],
            [-np.sin(pitch), 0, np.cos(pitch)]
        ],
        np.float32
    )  # Roll is ignored for the desired positioning

    # Convert rotation matrix to a 3x1 Rodrigues rotation vector
    plane_rvecs, _ = cv2.Rodrigues(rot_mat)

    # Emperically determined translation vectors
    plane_tvecs = np.array(
        [[150, 200, 1500]],
        np.float32
    )
    plane_tvecs = plane_tvecs.reshape((3, 1))

    return plane_rvecs, plane_tvecs


def get_chessboard_homography(camera_matrix, dist_coeffs):
    """Returns the homography for the virtual reality chessboard

    Args:
        camera_matrix (np.array): 3x3 camera matrix
        dist_coeffs (np.array): Distortion parameters

    Returns:
        np.array: 3x3 homography
    """
    plane_rvecs, plane_tvecs = get_chessboard_vecs()

    return homography_from_vecs(
        plane_rvecs,
        plane_tvecs,
        camera_matrix,
        dist_coeffs)


def camera_loop(pattern_width,
                pattern_height,
                pattern_shape,
                axis_lenght,
                calibration_filename):
    """Runs the virtual reality on the input of the video0 camera device

    Args:
        pattern_shape (int, int): Shape of the chessboard in number of squares
        pattern_width (int): Width of each square in mm
        pattern_height (int): Height of each square in mm
        axis_lenght (int): Lenght of the display axis in mm
        calibration_filename (str): Name of the file where the calibration will be read from
    """

    s = cv2.FileStorage(calibration_filename, cv2.FileStorage_READ)
    camera_matrix = s.getNode("camera_matrix").mat()
    dist_coeffs = s.getNode("dist_coeffs").mat()

    if (camera_matrix is None) and (dist_coeffs is None):
        print("File didn't contain camera_matrix and dist_coeffs")
        return

    world_points = get_object_points(
        pattern_shape, pattern_width, pattern_height)
    axis_points = get_axis(axis_lenght)
    # Inverted Axis for correct visualization
    inverted_axis_points = get_axis(axis_lenght, False)

    chessboard_homography = get_chessboard_homography(
        camera_matrix, dist_coeffs)
    chessboard = get_chessboard_pattern(
        pattern_shape, pattern_width, pattern_height)
    projected_chessboard = None

    cap = cv2.VideoCapture(0)
    while True:
        _, frame = cap.read()

        # Get the projected chessboard and its transformation vectors
        if projected_chessboard is None:
            projected_chessboard = cv2.warpPerspective(
                chessboard, chessboard_homography, (frame.shape[1], frame.shape[0]))

            plane_rvecs, plane_tvecs = get_chessboard_vecs()
            projected_axis_points, _ = cv2.projectPoints(
                axis_points, plane_rvecs, plane_tvecs, camera_matrix, dist_coeffs)
            projected_chessboard = draw_axis(
                projected_chessboard, projected_axis_points)
            plane_rvecs, _ = cv2.Rodrigues(plane_rvecs)

        # Undistort the frame
        frame = cv2.undistort(frame,
                              camera_matrix,
                              dist_coeffs)

        # Attempt to find the chessboard
        ret, corners = cv2.findChessboardCorners(frame, pattern_shape)
        if ret:
            # Get the transformation based on the found corners
            ret, rot_vecs, trans_vecs = cv2.solvePnP(
                world_points, corners, camera_matrix, dist_coeffs)

            # Project 3D axis to image plane
            image_points, _ = cv2.projectPoints(
                axis_points, rot_vecs, trans_vecs, camera_matrix, dist_coeffs)

            frame = draw_axis(frame, image_points)

            # Transform Camera->Plane to Plane-> Camera vectors
            rot_vecs, _ = cv2.Rodrigues(rot_vecs)

            # We are reverting the z axis definition when drawing so that it
            # comes out (up) not into (down) the plane. Do the same here to
            # match
            trans_vecs[2] = -trans_vecs[2]

            # Rotate the transform vector according to the plane and add it to
            # the plane displacement
            camera_tvecs = plane_tvecs + plane_rvecs @ trans_vecs

            # Rotate the camera vector according to the plane rotation
            camera_rvecs = plane_rvecs @ rot_vecs
            camera_rvecs, _ = cv2.Rodrigues(camera_rvecs)

            # Project 3D camera_axis to projected chessboard
            image_points, _ = cv2.projectPoints(
                inverted_axis_points, camera_rvecs, camera_tvecs, camera_matrix, dist_coeffs)

            # Draw the axis on the projected chessboard, copy necessary so that
            # different frames don't keep the old aixs
            projected_chessboard_with_axis = draw_axis(
                projected_chessboard.copy(), image_points)

            cv2.imshow("chessboard", projected_chessboard_with_axis)

        cv2.imshow('frame', frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='This program captures opens a series of files and calibrates the camera based on them.',
        add_help=False)
    parser.add_argument(
        '--pattern_width_elems', '-w',
        type=int,
        default=7,
        help='Width in number of elements for the pattern')
    parser.add_argument(
        '--pattern_height_elems', '-h',
        type=int,
        default=4,
        help='Height in number of elements for the pattern')
    parser.add_argument(
        '--pattern_width', '-W',
        type=int,
        default=30,
        help='Width of the each pattern element in mm')
    parser.add_argument(
        '--pattern_height', '-H',
        type=int,
        default=30,
        help='Height of each pattern element in mm')
    parser.add_argument(
        '--axis_lenght', '-a',
        type=int,
        default=100,
        help='Lenght of the virtual axis in mm')
    parser.add_argument(
        '--calibration_filename', '-c',
        type=str,
        help='Name of the file where the calibration will be saved')
    parser.add_argument(
        '--help',
        action='help',
        help='Show this help message and exit.')

    args = parser.parse_args()

    if args.calibration_filename:
        camera_loop(args.pattern_width,
                    args.pattern_height,
                    (args.pattern_width_elems,
                        args.pattern_height_elems),
                    args.axis_lenght,
                    args.calibration_filename,
                    )
    else:
        parser.print_help(sys.stderr)
