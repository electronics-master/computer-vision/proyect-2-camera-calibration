#!/usr/bin/env python3

import argparse
import os
import pathlib
import random
import sys

import cv2
import numpy as np

IMAGE_WAIT_TIME = 16


def compute_reprojection_errors(object_points,
                                image_points,
                                rot_vecs, trans_vecs,
                                camera_matrix, dist_coeffs):
    """Calculates the reproejection error for a given calibration

    Args:
        object_points (np.array): Nx3 array of 3D points in the world
        image_points (np.array): Nx2 array of 2D points in the image
        rot_vecs (np.array): Nx3x1 rotation vector for each transformation
        trans_vecs (np.array): Nx3X1 translation vector for each transformation
        camera_matrix (np.array): 3x3 Camera Matrix
        dist_coeffs (np.array): Distortion coefficients

    Returns:
        double: Mean reprojection error
    """
    mean_error = 0

    for corners, world_points, rot_vec, trans_vec in zip(
            image_points, object_points, rot_vecs, trans_vecs):
        projected_corners, _ = cv2.projectPoints(
            world_points, rot_vec, trans_vec, camera_matrix, dist_coeffs)
        error = cv2.norm(
            projected_corners,
            corners,
            cv2.NORM_L2)
        mean_error += error

    return mean_error / len(object_points)


def get_object_points(pattern_shape, pattern_width, pattern_height):
    """Returns a 2D grid corresponding to the object's 3D distribution

    Args:
        pattern_shape (int, int): Shape of the chessboard in number of squares
        pattern_width (int): Width of each square in mm
        pattern_height (int): Height of each square in mm

    Returns:
        [type]: [description]
    """

    real_world_points = np.zeros(
        (pattern_shape[0] * pattern_shape[1], 3), np.float32)
    real_world_points[:, :2] = np.mgrid[0:pattern_shape[0],
                                        0:pattern_shape[1]].T.reshape(-1, 2)

    # Scale so that it matches the pattern dimensions
    real_world_points[:, 0] *= pattern_width
    real_world_points[:, 1] *= pattern_height

    return real_world_points


def directory_read(image_names_file,
                   pattern_width,
                   pattern_height,
                   pattern_shape,
                   calibration_filename,
                   repetitions,
                   num_images):
    """Reads all the files from a directory and calibrates the camera based on them

    Args:
        image_names_file (str): File with the name of an image on each line
        reference_image_filename ([type]): Filename for the reference image
    """

    world_points = get_object_points(
        pattern_shape, pattern_width, pattern_height)

    object_points = []
    image_points = []
    # These two lists represent the same as the two above, but are meant for a comparision of calibration of points in object/image_points against the complete set
    total_object_points = []
    total_image_points = []

    if repetitions > 0:
        criteria = (cv2.TERM_CRITERIA_MAX_ITER, repetitions, sys.float_info.epsilon)
    else:
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, sys.float_info.epsilon)

    # Use for line order randomization
    random.seed(10)

    with open(image_names_file, "r") as name_file:
        print("Press q to skip to calibration")
        lines = name_file.readlines()

        if num_images > 0:
            random.shuffle(lines)

        for i, file_name in enumerate(lines):
            file_name = file_name.strip('\n')
            frame = cv2.imread(file_name)

            ret, corners = cv2.findChessboardCorners(frame, pattern_shape)
            if not ret:
                continue

            # Refine chessboard corners in grayscale
            grey = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            corners = cv2.cornerSubPix(
                grey, corners, (11, 11), (-1, -1), (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, sys.float_info.epsilon))

            # Append a real world coordinates and image coordinates to the
            # corresponding vectors
            if num_images < 1:
                object_points.append(world_points)
                image_points.append(corners)
            else:
                if i < num_images:
                    object_points.append(world_points)
                    image_points.append(corners)
                total_object_points.append(world_points)
                total_image_points.append(corners)

            # Draw and display the corners
            cv2.drawChessboardCorners(frame, pattern_shape, corners, ret)

            cv2.imshow('frame', frame)
            key = cv2.waitKey(IMAGE_WAIT_TIME) & 0xFF
            if key == ord('q'):
                break

    print("Starting calibration")
    if (len(object_points) != 0) and (len(image_points) != 0):
        init_camera_matrix = cv2.initCameraMatrix2D(
            object_points, image_points, frame.shape[0:2], 0)

        print("Calibrating with {} images".format(len(object_points)))

        ret, camera_matrix, dist_coeffs, rot_vecs, trans_vecs = cv2.calibrateCamera(
            object_points,
            image_points,
            frame.shape[0:2],
            init_camera_matrix,
            None,
            flags=cv2.CALIB_USE_INTRINSIC_GUESS,
            criteria=criteria)

        if num_images < 1:
            print("Calibrating with {} images".format(len(object_points)))
            mean_error = compute_reprojection_errors(object_points,
                                                    image_points,
                                                    rot_vecs, trans_vecs,
                                                    camera_matrix, dist_coeffs)
        else:
            print("Reprojection with {} images".format(len(total_object_points)))
            rot_vecs = []
            trans_vecs = []
            for object_points, image_points in zip(total_object_points, total_image_points):
                _, rot_vec, trans_vec = cv2.solvePnP(
                    object_points, image_points, camera_matrix, dist_coeffs)
                rot_vecs.append(rot_vec)
                trans_vecs.append(trans_vec)

            mean_error = compute_reprojection_errors(total_object_points,
                                                    total_image_points,
                                                    rot_vecs, trans_vecs,
                                                    camera_matrix, dist_coeffs)

        print("mean reprojection error: {}".format(mean_error))

        s = cv2.FileStorage(calibration_filename, cv2.FileStorage_WRITE)
        s.write("camera_matrix", camera_matrix)
        s.write("dist_coeffs", dist_coeffs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='This program captures opens a series of files and calibrates the camera based on them.',
        add_help=False)
    parser.add_argument(
        '--image_names_file',
        '-t',
        type=str,
        help='Filename containing the image path of the files to be used for calibration')
    parser.add_argument(
        '--pattern_width_elems', '-w',
        type=int,
        default=7,
        help='Width in number of elements for the pattern')
    parser.add_argument(
        '--pattern_height_elems', '-h',
        type=int,
        default=4,
        help='Height in number of elements for the pattern')
    parser.add_argument(
        '--pattern_width', '-W',
        type=int,
        default=30,
        help='Width of the each pattern element in mm')
    parser.add_argument(
        '--pattern_height', '-H',
        type=int,
        default=30,
        help='Height of each pattern element in mm')
    parser.add_argument(
        '--calibration_filename', '-o',
        type=str,
        help='Name of the file where the calibration will be saved')
    parser.add_argument(
        '--help',
        action='help',
        help='Show this help message and exit.')
    parser.add_argument(
        '--repetitions', '-r',
        type=int,
        default=-1,
        help='Number of repetitions for the calibration, if this is ignored the default value will be used.')
    parser.add_argument(
        '--num_images', '-m',
        type=int,
        default=-1,
        help='Number of images to be used for the calibration, if ignored it will use all images.')

    args = parser.parse_args()

    if args.image_names_file and args.calibration_filename:
        directory_read(args.image_names_file,
                       args.pattern_width,
                       args.pattern_height,
                       (args.pattern_width_elems,
                        args.pattern_height_elems),
                       args.calibration_filename,
                       args.repetitions,
                       args.num_images
                       )
    else:
        parser.print_help(sys.stderr)
