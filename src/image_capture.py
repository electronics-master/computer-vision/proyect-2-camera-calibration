#! /usr/bin/env python3

import argparse
import os
import pathlib
import sys

import cv2


def camera_loop(
        save_dir,
        filenames_file,
        pattern,
        pattern_shape):
    """Function that reads from the camera and saves the images

    Args:
        save_dir (str): Save directory for the save image functionality.
        filenames_file (str): Name of the file where all the image names will be saved.
    """
    cap = cv2.VideoCapture(0)

    # Check and create directory if needed
    if save_dir:
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)

    frame_count = 0
    print("Press q to exit")
    while True:
        # Capture frame-by-frame
        _, frame = cap.read()

        # Attempt to find the pattern
        if pattern == "circles":
            # Circle pattern is only used in this stage and kept for testing
            # reasons
            ret, image_points = cv2.findCirclesGrid(frame, pattern_shape)
        elif pattern == "chessboard":
            ret, image_points = cv2.findChessboardCorners(frame, pattern_shape)
        else:
            ret = False

        # Save the image if there is a valid pattern
        if ret and image_points.size:
            image_filename = "img_" + str(frame_count) + ".png"
            frame_count = frame_count + 1

            cv2.imwrite(
                os.path.join(
                    save_dir,
                    image_filename),
                frame)
            with open(os.path.join(save_dir, filenames_file + ".txt"), "a") as name_file:
                name_file.write(
                    os.path.join(
                        pathlib.Path().absolute(),
                        save_dir, image_filename) +
                    "\n")

        # Display the resulting frame
        cv2.drawChessboardCorners(frame, pattern_shape, image_points, ret)

        cv2.imshow('frame', frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='This program captures a series of images to a directory',
        add_help=False)
    parser.add_argument(
        '--save_dir',
        '-d',
        type=str,
        help='When capturing the program will save all the images to this directory with the corresponding route')
    parser.add_argument(
        '--image_filename',
        '-t',
        type=str,
        help='The saved image names will be saved to this filename')
    parser.add_argument(
        '--pattern', '-p',
        choices=["chessboard", "circles"],
        default="chessboard",
        help='Select an option for the pattern')
    parser.add_argument(
        '--pattern_width', '-w',
        type=int,
        default=7,
        help='Width in number of elements for the pattern')
    parser.add_argument(
        '--pattern_height', '-h',
        type=int,
        default=4,
        help='Height in number of elements for the pattern')
    parser.add_argument(
        '--help',
        action='help',
        help='Show this help message and exit.')
    args = parser.parse_args()

    if args.save_dir and args.image_filename:
        camera_loop(
            args.save_dir,
            args.image_filename,
            args.pattern,
            (args.pattern_width,
             args.pattern_height))
    else:
        parser.print_help(sys.stderr)
